package com.electron.d2json

import com.electron.d2json.extensions.file.addSeparator
import com.electron.d2json.parsers.ParserHandler
import com.electron.d2json.parsers.enum.EnumsParser
import com.electron.d2json.parsers.message.MessagesParser
import com.electron.d2json.parsers.type.TypesParser
import com.github.ajalt.mordant.TermColors
import com.jpexs.decompiler.flash.SWF
import com.jpexs.decompiler.flash.tags.DoABC2Tag
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.measureTimeMillis

fun showUsage() {
    println("Usage: d2json <input> <output>")
    println("""Parameters:
        |   input      The input DofusInvoker.swf you want to extract messages/types/enums.
        |   output     The output path where the messages, types, and enums will be saved.
    """.trimMargin())
}

fun main(args: Array<String>) {
    // <input> <output> => 2
    if (args.size != 2) {
        showUsage()
        return
    }

    // Variables
    val t = TermColors()
    val inputFile: String = args[0]
    var outputPath: String = args[1]
    println(t.brightYellow("Input file: $inputFile"))

    // Checks
    val outputFile = File(outputPath)
    if (outputFile.isFile) {
        println(t.red("The output path cannot be a file !"))
        return
    }

    // Create directories if they don't exist
    if (!outputFile.exists()) {
        println(t.cyan("$outputPath doesn't exist. We create the directory for you !"))
        outputPath = outputPath.addSeparator()
        try {
            Files.createDirectories(Paths.get(outputPath))
        } catch (e: IOException) {
            println(t.red("Cannot create directories - $e"))
        }
    }

    // Try to parse the SWF File
    var swfFile: SWF? = null
    try {
        val elapsed: Long = measureTimeMillis {
            swfFile = SWF(FileInputStream(inputFile), false)
        }
        println(t.green("File parsed in $elapsed ms"))
    } catch (e: Exception) {
        println(t.red("The input file is not valid ! - $e"))
        return
    }

    // Try to get the DoABCTag
    val abcTag: DoABC2Tag? = swfFile!!.tags.firstOrNull { it is DoABC2Tag } as DoABC2Tag?
    if (abcTag == null) {
        println(t.red("Can't find the DoABCTag !"))
        return
    }
    val as3Data = abcTag.abc

    // Create a ParserHandler and parse the datas
    val parserHandler = ParserHandler()
    parserHandler.register(EnumsParser(as3Data, "${outputPath}enums.json"))
    parserHandler.register(TypesParser(as3Data, "${outputPath}types.json"))
    parserHandler.register(MessagesParser(as3Data, "${outputPath}messages.json"))
    parserHandler.run()
}