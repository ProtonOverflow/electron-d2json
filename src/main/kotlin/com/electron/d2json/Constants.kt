package com.electron.d2json

object Constants {
    /**
     * Where are located enums
     */
    const val ENUM_PATH = "com.ankamagames.dofus.network.enums"

    /**
     * Where are located types classes
     */
    const val TYPE_PATH = "com.ankamagames.dofus.network.types"

    /**
     * Where are located messages classes
     */
    const val MESSAGE_PATH = "com.ankamagames.dofus.network.messages"
}