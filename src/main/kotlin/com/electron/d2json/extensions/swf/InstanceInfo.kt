package com.electron.d2json.extensions.swf

import com.jpexs.decompiler.flash.abc.ABC
import com.jpexs.decompiler.flash.abc.avm2.AVM2ConstantPool
import com.jpexs.decompiler.flash.abc.types.ClassInfo
import com.jpexs.decompiler.flash.abc.types.InstanceInfo

/**
 * Get the namespace of the target InstanceInfo
 *
 * @param constantPool the ABC's constant pool which is useful for retrieving information
 * @return the namespace
 */
fun InstanceInfo.getNamespaceName(constantPool: AVM2ConstantPool): String =
        this.getName(constantPool).getNamespace(constantPool).getName(constantPool).toRawString()

/**
 * Get the class name of the target InstanceInfo
 *
 * @param constantPool the ABC's constant pool which is useful for retrieving information
 * @return the class name
 */
fun InstanceInfo.getClassName(constantPool: AVM2ConstantPool): String =
        this.getName(constantPool).getName(constantPool, null, false, false)

/**
 * Convert the InstanceInfo into a ClassInfo
 *
 * @param as3 the instance info's ABC
 * @return the class info
 */
fun InstanceInfo.toClassInfo(as3: ABC): ClassInfo =
        as3.class_info[as3.instance_info.indexOf(this)]