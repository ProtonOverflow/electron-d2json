package com.electron.d2json.extensions.swf

import com.jpexs.decompiler.flash.abc.avm2.AVM2ConstantPool
import com.jpexs.decompiler.flash.abc.types.ValueKind
import com.jpexs.decompiler.flash.abc.types.traits.Trait
import com.jpexs.decompiler.flash.abc.types.traits.TraitSlotConst

/**
 * Get the trait's name
 *
 * @param constantPool the trait's ABC's constant pool
 * @return the trait's name
 */
fun Trait.getName(constantPool: AVM2ConstantPool): String =
        constantPool.getMultiname(this.name_index).getName(constantPool, null, false, false)

/**
 * Try to get the TraitSlotConst's value
 *
 * @param constantPool the trait's ABC's constant pool
 * @return the value (can be null) and the type's name
 */
fun TraitSlotConst.getValue(constantPool: AVM2ConstantPool): Pair<Any?, String> {
    val typeName = this.getType(constantPool, null)
    return when (this.value_kind) {
        ValueKind.CONSTANT_DecimalOrFloat -> Pair(constantPool.getDecimal(this.value_index), typeName)
        ValueKind.CONSTANT_Int -> Pair(constantPool.getInt(this.value_index), typeName)
        ValueKind.CONSTANT_UInt -> Pair(constantPool.getUInt(this.value_index), typeName)
        ValueKind.CONSTANT_Double -> Pair(constantPool.getDouble(this.value_index), typeName)
        ValueKind.CONSTANT_False -> Pair(false, typeName)
        ValueKind.CONSTANT_True -> Pair(true, typeName)
        else -> Pair(null, typeName)
    }
}