package com.electron.d2json.extensions.file

import java.io.File

/**
 * Add a separator at the end of the string if it hasn't one
 *
 * @return the string with, or not, the separator at the end
 */
fun String.addSeparator(): String {
    if (!this.endsWith(File.separatorChar)) {
        return "$this${File.separatorChar}"
    }
    return this
}