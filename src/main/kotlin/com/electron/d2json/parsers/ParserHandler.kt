package com.electron.d2json.parsers

import com.github.ajalt.mordant.TermColors
import kotlin.system.measureTimeMillis

/**
 * A simple class which execute the parsers' tasks
 */
class ParserHandler {

    private val parsers: MutableList<Parser> = mutableListOf()

    /**
     * Register a parser which will parse the ABC and save the data to the output file
     *
     * @param parser the target parser
     */
    fun register(parser: Parser) {
        println("${parser.name} is registered !")
        parsers.add(parser)
    }

    /**
     * Execute all parsers' tasks
     */
    fun run() {
        for (i in 0..parsers.size) {
            with(TermColors()) {
                println()
                println("${brightCyan("Task ${i+1}/${parsers.size}")}")

                // We measure the time that the parser takes to prepare its variables
                var elapsed = measureTimeMillis {
                    parsers[i].prepare()
                }
                println("${brightYellow("Parsed in $elapsed ms")}")

                // We measure the time that the parser takes to save the data to the file
                elapsed = measureTimeMillis {
                    parsers[i].finish()
                }
                println("${brightYellow("Saved in $elapsed ms")}")

                println("${brightGreen("Task ${i+1}/${parsers.size} finished")}")
            }
        }
    }

}