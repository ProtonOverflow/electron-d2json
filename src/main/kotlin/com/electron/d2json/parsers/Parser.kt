package com.electron.d2json.parsers

import com.jpexs.decompiler.flash.abc.ABC

abstract class Parser(val as3: ABC, val output: String) {
    /**
     * @return the name of the parser
     */
    abstract val name: String

    /**
     * Prepare the parser's variables
     */
    abstract fun prepare()

    /**
     * Save the data to the output file in JSON format
     */
    abstract fun finish()
}