package com.electron.d2json.parsers.enum

import com.squareup.moshi.Json

/**
 * Enum data class
 *
 * @property name the enum's name
 * @property fullPath the full path of the enum, that is to say: Namespace+ClassName
 * @property values the constants
 */
data class  Enum(
        val name: String,
        @Json(name = "full_path") val fullPath: String,
        val values: List<EnumValue>
)

/**
 * It represents a constant in the enum
 *
 * @property name the constant's name
 * @property type the constant's type
 * @property value the constant's value
 */
data class EnumValue(
        val name: String,
        val type: String,
        val value: Any
)