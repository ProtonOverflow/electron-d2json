package com.electron.d2json.parsers.enum

import com.electron.d2json.Constants
import com.electron.d2json.extensions.swf.*
import com.electron.d2json.parsers.Parser
import com.jpexs.decompiler.flash.abc.ABC
import com.jpexs.decompiler.flash.abc.avm2.AVM2ConstantPool
import com.jpexs.decompiler.flash.abc.types.InstanceInfo
import com.jpexs.decompiler.flash.abc.types.traits.TraitSlotConst

/**
 * Parse the ABC in order to retrieve enums and to save them in the output file in JSON format
 *
 * @property as3 the target ABC
 * @property output the output file where the data will be saved
 */
class EnumsParser(as3: ABC, output: String) : Parser(as3, output) {

    private val enums: MutableList<Enum> = mutableListOf()
    private var constantPool: AVM2ConstantPool = as3.constants

    /**
     * @return the name of the parser
     */
    override val name: String
        get() = "EnumsParser"

    /**
     * Retrieve the enums and their constants
     *
     * @exception NotImplementedError if the trait is not a TraitSlotConst
     */
    override fun prepare() {
        // We retrieve the instances which are only enums
        val enumsInstances = as3.instance_info.filter { it.isEnum() }
        for (instance in enumsInstances) {
            // We convert the instance to a class in order to get the traits
            val classInfo = instance.toClassInfo(as3)
            val values: MutableList<EnumValue> = mutableListOf()
            for (trait in classInfo.static_traits.traits) {
                // The trait must be a TraitSlotConst since enum is only constants
                if (trait !is TraitSlotConst) {
                    throw NotImplementedError()
                }

                // Get the constant's name
                val name = trait.getName(constantPool)
                // Get the constant's value and constant's type
                val (value, typeName) = trait.getValue(constantPool)

                values.add(EnumValue(name, typeName, value!!))
            }
            // Get the instance's name
            val name = instance.getClassName(constantPool)
            // Get the instance's full path (Namespace + ClassName)
            val fullPath = instance.getName(constantPool).getNameWithNamespace(constantPool, false).toPrintableString(true)
            enums.add(Enum(name, fullPath, values))
        }
        println("${enums.size} enums parsed")
    }

    /**
     * Save the enums to the output file in JSON format
     */
    override fun finish() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Check if the target instance is an enum
     */
    private fun InstanceInfo.isEnum(): Boolean =
            this.getNamespaceName(constantPool).startsWith(Constants.ENUM_PATH)
}