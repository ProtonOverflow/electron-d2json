package com.electron.d2json.parsers.type

import com.squareup.moshi.Json

/**
 * Type data class
 *
 * @property name the name of the type class
 * @property fullPath the full path of the type class, that is to say: Namespace + ClassName
 * @property protocolId the protocol id of the type
 * @property extends
 * @property implements
 * @property fields the class' fields
 */
data class Type(
        val name: String,
        @Json(name = "full_path") val fullPath: String,
        @Json(name = "protocol_id") val protocolId: Int,
        var extends: String,
        var implements: String,
        val fields: List<TypeField>
)

/**
 * It represents a field in the type's class
 *
 * @property name the name of the field
 * @property type the type of the field
 */
data class TypeField(
        val name: String,
        val type: String
)