package com.electron.d2json.parsers.type

import com.electron.d2json.parsers.Parser
import com.jpexs.decompiler.flash.abc.ABC

/**
 * Parse the ABC in order to retrieve types classes and to save them in the output file in JSON format
 *
 * @property as3 the target ABC
 * @property output the output file where the data will be saved
 */
class TypesParser(as3: ABC, output: String): Parser(as3, output) {

    /**
     * @return the name of the parser
     */
    override val name: String
        get() = "TypesParser"

    /**
     * Retrieve the types classes
     */
    override fun prepare() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Save the types classes to the output file in JSON format
     */
    override fun finish() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}