package com.electron.d2json.parsers.message

import com.squareup.moshi.Json

/**
 * Message data class
 *
 * @property name the name of the message class
 * @property fullPath the full path of the message class, that is to say: Namespace + ClassName
 * @property protocolId the protocol id of the type
 * @property extends
 * @property implements
 * @property fields the class' fields
 */
data class Message(
        val name: String,
        @Json(name = "full_path") val fullPath: String,
        @Json(name = "protocol_id") val protocolId: Int,
        var extends: String,
        var implements: String,
        val fields: List<MessageField>
)

/**
 * It represents a field in the message's class
 *
 * @property name the name of the field
 * @property type the type of the field
 */
data class MessageField(
        val name: String,
        val type: String
)