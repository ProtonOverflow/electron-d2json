package com.electron.d2json.parsers.message

import com.electron.d2json.parsers.Parser
import com.jpexs.decompiler.flash.abc.ABC

/**
 * Parse the ABC in order to retrieve messages classes and to save them in the output file in JSON format
 *
 * @property as3 the target ABC
 * @property output the output file where the data will be saved
 */
class MessagesParser(as3: ABC, output: String): Parser(as3, output) {

    /**
     * @return the name of the parser
     */
    override val name: String
        get() = "MessagesParser"

    /**
     * Retrieve the messages classes
     */
    override fun prepare() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Save the messages classes to the output file in JSON format
     */
    override fun finish() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}